﻿set xact_abort on 
set nocount on

declare @company int , @user int  ,@token varchar(100)
set @token =  '{token}'

select @company = login_company_key , @user = login_user_key
from y_login l
inner join y_company c on l.login_company_key = c.company_key
inner join y_user u on l.login_user_key = u.user_key 
where token =@token
--and datediff(Hour, l.update_time , getdate()) < 5


;if @user is null
begin
	raiserror( '无效的token',16,1)
end
else
begin
	update y_login set update_time = getdate() where token = @token
	select @company  as company_key , @user as user_key
end