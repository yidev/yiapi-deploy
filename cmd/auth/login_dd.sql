﻿declare  @token varchar(50) , @platform int, @dd_user varchar(100) 
set @platform = {platform}
set @dd_user = '{dd_user}'

set @token = replace(newid(),'-','') 

INSERT INTO [dbo].[y_login]([login_company_key],[login_user_key],[create_time],[update_time],[token])
select u.company_key , user_key , getdate(), getdate(), @token
from y_platform p 
inner join y_company c on p.company_key = c.company_key 
inner join y_platform_user  u on p.platform_key = u.platform_key 
where u.platform_user_id = @dd_user  and u.platform_key =  @platform

select @token as token